'use strict';

const { Logger } = require('../../../build/Logger');
const { LogLevel } = require('../../../build/constants/LogLevel');

const checkDelegate = (name, expectedLogLevel) => {
	const tested = new Logger();

	tested.log = sinon.fake();

	const mockData = {
		message: 'TestMessage',
		uniqueMarker: 'TestUniqueMarker',
		extra: {},
	};

	tested[name](mockData.message, mockData.uniqueMarker, mockData.extra);

	sinon.assert.calledOnce(tested.log);

	expect(tested.log.args[0][0], 'to be', expectedLogLevel);
	expect(tested.log.args[0][1], 'to be', mockData.message);
	expect(tested.log.args[0][2], 'to be', mockData.uniqueMarker);
	expect(tested.log.args[0][3], 'to be', mockData.extra);
};

[
	LogLevel.ERROR,
	LogLevel.WARN,
	LogLevel.INFO,
	LogLevel.DEBUG,
	LogLevel.TRACE,
].forEach(level => {
	describe(`#${level.toLowerCase()}`, function() {
		it('should forward all parameters and attach the correct log level', function() {
			checkDelegate(level.toLowerCase(), level);
		});
	});
});
