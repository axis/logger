'use strict';

const { Color } = require('../../../../build/util/Color');

describe('#setEnabled', function() {
	const getClean = () => {
		const tested = new Color();

		return tested;
	};

	it('should set the disabled status according to the input', function() {
		const tested = getClean();

		tested.setEnabled(true);
		expect(tested.disabled, 'to be false');

		tested.setEnabled(false);
		expect(tested.disabled, 'to be true');
	});
});
